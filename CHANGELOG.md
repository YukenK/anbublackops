#v0.4.0:

* Added wall-running.
* Added in attacks (basic, trip (down + attack), uppercut (up + attack)).
* Removed borders from chat.
* Health/Stamina/Energy is now displayed in the top-left corner of the screen.

#v0.3.1:

* Fixed an issue with skills not working properly as function execution doesn't get paused by threads, fixed this issue.

#v0.3.0:

* Added in health, energy & stamina bars which are only visible to allies.
* Created a skill system, which allows for charged & held skills, skill banning, etc.
* Moved some character selection stuff around, made it more efficient in general.
* Clients will now reattach to their mob if they log out and log back in.

#v0.2.0:

* Rounds changed to a match manager, allows for multiple matches per server.

#v0.1.0:

* Character selection finished, can get past the main menu into a map & move around, chat to players.
* Added in binds (which can stop movement and/or turning).
* Finished a basic buff system.

#v0.0.1:

* Initial commit with basic game systems & character selection in. No actual gameplay yet.