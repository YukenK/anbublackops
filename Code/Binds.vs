bind
	var canTurn
	var endTime
	var damageEnd
	var iconState
	var bindID
	onNew(_time, _turn, _icon, _damageEnd, _iconState, _id)
		this.endTime = _time + Date.now()
		this.canTurn = _turn
		this.damageEnd = _damageEnd
		this.iconState = _iconState
		this.bindID = _id
		if(_icon)
			this.atlasName = _icon[0]
			this.iconName = _icon[1]
Mob
	var binds = []
	function getBind(_id)
		foreach(var B in this.binds)
			if(B.id == _id)
				return B
	function addBind(_time, _turn, _icon, _damageEnd, _iconState, _id)
		var B = new Diob('bind', _time, _turn, _icon, _damageEnd, _iconState, _id)
		this.binds.push(B)
		if(_icon)
			this.addOverlay(B)
		return B
	function removeBind(B)
		this.binds.splice(B, 1)
		if(B.atlasName)
			this.removeOverlay(B)
	function bindCheck()
		foreach(var B in this.binds)
			if(B.endTime != -1 && Date.now() >= B.endTime)
				this.removeBind(B)