event
	knockBack
		doesTick = 1
		eventID = "knockBack"
		tickTime = 50
		endTick = 5
		var knockX
		var knockY
		var forceVel
		function startTick()
			forceVel = new Object('vector', this.knockX, this.knockY)
			foreach(var P in this.owners)
				P.forceVelocities.push(this.forceVel)
				P.addBind(300, false, null, false, null, "stun")
			Type.callFunction(this.parentType, 'startTick', this)
		function setOther(_other)
			this.knockX = _other[0]
			this.knockY = _other[1]
		function endTick()
			foreach(var P in this.owners)
				P.forceVelocities.splice(this.forceVel)
			Type.callFunction(this.parentType, 'endTick', this)
			
Mob
	var knock
	var trip
	var nextKnock
	var nextTrip
	var attackRegen
	function canAttack()
		if(this.stamina < 5)
			return false
		return this.canMove()
	function canHit(M)
		if(this.team != M.team && !M.dead)
			return true
		return false
	function Attack()
		if(this.canAttack())
			this.addBind(this.attackSpeed, false, null, false, null, "attack")
			this.attackRegen = Date.now() + this.attackSpeed
			this.staminaChange(-5)
			foreach(var P in this.front(this.attackRange, true))
				if(this.canHit(P))
					if(this.trip && !this.knock && Date.now() >= this.nextTrip)
						this.playAnimation(this.atlasName, this.iconName, "trip")
						P.addBind(350, false, null, false, null, "stun")
						this.nextTrip = Date.now() + 3500
					else if(this.knock && !this.trip && Date.now() >= this.nextKnock)
						this.playAnimation(this.atlasName, this.iconName, "knock")
						P.addEvent('event/knockBack', [P], true, 5, -1, false, 50, [this.knockX, this.knockY])
						this.nextKnock = Date.now() + 3500
					else
						this.playAnimation(this.atlasName, this.iconName, Util.pick(["attack1", "attack2"]))
					P.healthChange(-this.attack, true, this)