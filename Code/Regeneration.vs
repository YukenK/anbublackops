Mob
	var health = 100
	var maxHealth = 100
	var energy = 100
	var maxEnergy = 100
	var stamina = 100
	var maxStamina = 100
	var healthRegen
	var energyRegen
	var staminaRegen
	var healthBar
	var energyBar
	var staminaBar
	var dead = false
	function canRegen(_type)
		if(this.isRun && this.canRun())
			return false
		if(this.getEvent("wallRun"))
			return false
		if(_type == "STAMINA" && this.attackRegen > Date.now())
			return false
		return !this.dead
	function Regeneration()
		if(this.canRegen("HEALTH"))
			this.health = clamp(this.health + this.healthRegen, 0, this.maxHealth)
		if(this.canRegen("ENERGY"))
			this.energy = clamp(this.energy + this.energyRegen, 0, this.maxEnergy)
		if(this.canRegen("STAMINA"))
			this.stamina = clamp(this.stamina + this.staminaRegen, 0, this.maxStamina)
		this.setBars()
	function setBars()
		this.staminaBar.iconState = Math.round(this.stamina / this.maxStamina * 21)
		this.energyBar.iconState = Math.round(this.energy / this.maxEnergy * 21)
		this.healthBar.iconState = Math.round(this.health / this.maxHealth * 21)
	function healthChange(i, _deathCheck = 1, _hitter)
		this.health = clamp(this.health + i, 0, this.maxHealth)
		if(_deathCheck && i < 0)
			this.deathCheck(_hitter)
		foreach(var B in this.binds)
			if(B.damageEnd)
				this.binds.splice(B)
	function energyChange(i)
		this.energy = clamp(this.energy + i, 0, this.maxEnergy)
	function staminaChange(i)
		this.stamina = clamp(this.stamina + i, 0, this.maxStamina)
	function deathCheck(_hitter)
		if(this.health <= 0)
			this.dead = true
			var text
			foreach(var P in this.deathPassives)
				P.Cast(this, _hitter)
			if(_hitter.baseType == "Mob")
				foreach(var P in _hitter.killPassives(_hitter, this))
			if(getHitter(_hitter))
				text = "<font color=red>" + this.charName + " has been killed by " + getHitter(_hitter).charName + ".</font>"
			else
				text = "<font color=red>" + this.charName + " has died.</font>"
			foreach(var P in matchManager.getMatch(client.matchID).players)
				if(P.client)
					P.client.showText(text)
			matchManager.getMatch(client.matchID).alivePlayers.splice(this, 1)
function getHitter(_hitter)
	switch(_hitter.baseType)
		case "Mob":
			return _hitter