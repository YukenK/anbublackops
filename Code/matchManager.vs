Client
	var matchID = "main"
matchManager
	var matches = []
	function getMatch(_id, _map)
		foreach(var M in this.matches)
			if(M.matchID == _id || M.mapName == _map)
				return M
	function newPlayer(_id, player)
		var M = this.getMatch(_id)
		M.alivePlayers.push(player)
		M.players.push(player)
		player.matchID = _id
		switch(player.team)
			case "hero":
				M.teams[0]++
				break
			case "villain":
				M.teams[1]++
				break
			case "akatsuki":
				M.teams[2]++
				break
		M[player.team + "Bars"].push(player.healthBar, player.energyBar, player.staminaBar)
		foreach(var P in M.players)
			foreach(var B in M[P.team + "Bars"])
				if(P.getInvisibilityViewers().indexOf(B) == -1)
					P.addInvisibilityViewer(B)
	function newMatch(_id)
		this.matches.push(new Object('match', _id, "World"))
	function teamAlive(match)
		if(match.mode == "LSS")
			if(match.teams[0] && !match.teams[1] && !match.teams[2])
				return "Heroes"
			else if(match.teams[1] && !match.teams[0] && !match.teams[2])
				return "Villains"
			else if(match.teams[2] && !match.teams[0] && !match.teams[1])
				return "Akatsuki"
			else if(!match.teams[0] && !match.teams[1] && !match.teams[2])
				return "Admins"
		else if(match.mode == "FFA")
			if(match.alivePlayers.length < 1)
				return "No one"
			else if(match.alivePlayers.length == 1)
				foreach(var M in match.alivePlayers)
					return M.charName
		return true
	function roundEnd(match)
		if(this.teamAlive(match) != true)
			var endTime = Date.now() + 5000
			var print = 0
			Event.interruptThread(match.roundThread)
			match.roundThread = thread(50) // Loop until the round's end time or until it should stop ending the round.
				if(this.teamAlive(match) == true)
					foreach(var P in match.players)
						if(P.client)
							P.client.showText("Somebody has come back to life, the round continues!")
					Event.interruptThread(match.roundThread)
				else if(print == 0)
					print++
					foreach(var P in match.players)
						if(P.client)
							if(mode == "LSS")
								P.client.showText(teamAlive() + " have won the round!")
							else if(mode == "FFA")
								P.client.showText(teamAlive() + " has won the round!")
				if(Date.now() >= endTime)
					this.newRound(match)
					Event.interruptThread(match.roundThread)
	function newRound(match)
		foreach(var M in eventPlayers)
			if(M.matchID == match.matchID)
				eventPlayers.splice(M, 1)
		match.selectedHeroes = []
		match.teams = [0, 0, 0]
		foreach(var P in match.players)
			foreach(var B in P.getInvisibilityViewers())
				P.removeInvisibilityViewer(B)
			P.resetCharacter()
			if(P.client)
				P.client.resetScreens()
				P.client.switchScreen(P.currentScreen, "teamScreen")
				P.client.canChat = false
			P.selectedHero = false
			P.setLoc(0, 0, "selectMap")
match
	var matchID
	var matchOwner
	var players = []
	var selectedHeroes = []
	var alivePlayers = []
	var heroBars = [] // List of team-specific UI elements.
	var villainBars = []
	var akatsukiBars = []
	var map = "devTest"
	var mapName
	var spawns = []
	var roundThread
	var teamBalance = 1
	var mode = "LSS"
	var teams = [0, 0, 0]
	onNew(_id, _owner)
		this.matchID = _id
		this.mapName = this.map + _id
		Map.cloneMap("devTest", "devTest" + this.matchID)
	