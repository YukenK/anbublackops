Mob
	var baseStats = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] // Same order as the stats in Characters. Base stats to use for buffs. All stats max out at 2x or 0.5x their base values, so you can't select 4 different heroes who buff max HP and get 1,000 HP.
	var mults = [1, 1, 1] // CD, Heal, Damage multipliers.
	var buffs = []
	function addBuff(_time, _stats, _icon)
		var B = new Diob('buff', _time, _stats, _icon)
		this.buffs.push(B)
		if(_icon)
			this.addOverlay(B)
	function buffCheck()
		foreach(var B in this.buffs)
			if(B.endTime != -1 && Date.now() >= B.endTime)
				this.removeBuff(B)
	function removeBuff(B)
		this.buffs.splice(B, 1)
		if(B.atlasName)
			this.removeOverlay(B)
buff
	var stats = []
	var mults = []
	var endTime
	onNew(_time, _stats, _icon)
		endTime = _time
		stats  = _stats
		if(_icon)
			this.atlasName = icon[0]
			this.iconName = icon[1]