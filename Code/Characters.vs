var roundThread
var teams = [0, 0, 0]
spawnPoints
	var team
	var map
	hero
		team = "hero"
	villain
		team = "villain"
	akatsuki
		team = "akatsuki"
	onNew()
		var T = thread(250)
			if(matchManager.getMatch(null, this.getMapName()))
				matchManager.getMatch(null, this.getMapName()).spawns.push(this)
				Event.interruptThread(T)
Mob
	function setStats(_stats)
		this.attack = _stats[0]
		this.attackSpeed = _stats[1]
		this.attackRange = _stats[2]
		this.maxHealth = _stats[3]
		this.healthRegen = _stats[4]
		this.maxEnergy = _stats[5]
		this.energyRegen = _stats[6]
		this.maxStamina = _stats[7]
		this.staminaRegen = _stats[8]
		this.moveSpeed = _stats[9]
		this.runSpeed = _stats[10]
		this.knockX = _stats[11]
		this.knockY = _stats[12]
	function maxHeal()
		this.health = this.maxHealth
		this.energy = this.maxEnergy
		this.stamina = this.maxStamina
	function resetCharacter()
		this.skills = []
		this.events = []
		this.buffs = []
		this.binds = []
		this.passives = []
		this.deathPassives = []
		this.killPassives = []
		this.meleePassives = []
		this.hitPassives = []
	function spawnPoint()
		var spawnAt = []
		foreach(var S in matchManager.getMatch(this.client.matchID).spawns)
			if(matchManager.getMatch(this.client.matchID).mode == "FFA" && S.map == matchManager.getMatch(this.client.matchID).map)
				spawnAt.push(S)
			else if(matchManager.getMatch(this.client.matchID).mode == "LSS" && S.team == this.team && S.getMapName() == matchManager.getMatch(this.client.matchID).mapName)
				this.setLoc(S)
		if(mode == "FFA")
			this.setLoc(Util.pick(spawnAt))
Client
	function selectHero(charName)
		if(matchManager.getMatch(this.matchID).selectedHeroes.indexOf(charName) != -1)
			return false
		var C = new Object('character/' + charName)
		if(C.canSelect(this))
			matchManager.getMatch(this.matchID).selectedHeroes.push(charName)
			this.selectedHero = true
			this.mob.atlasName = "characterIcons"
			this.mob.iconName = charName
			this.mob.team = C.team
			this.mob.charName = C.charName
			foreach(var S in C.skills)
				this.mob.skills.push(new Object('skill/' + S))
			this.mob.setStats(C.stats)
			this.mob.maxHeal()
			this.mob.spawnPoint()
			matchManager.newPlayer(this.matchID, this.mob)
			eventPlayers.push(this.mob)
			foreach(var M in matchManager.getMatch(this.matchID).players)
				if(M.client.currentScreen == C.team + "Screen")
					var T = M.client.getInterfaceElement(M.client.currentScreen, charName)
					T.selectDisplay.setTransition({'alpha': 0.6}, 8, 20)
					T.overDisplay.setTransition({'alpha': 0})
			this.hideInterface("teamScreen")
			this.hideInterface("akatsukiScreen")
			this.hideInterface("heroScreen")
			this.hideInterface("villainScreen")
			this.hideInterface("backgroundInterface")
			this.showInterface("Interface")
			this.showInterface("HUD")
			if(muted.indexOf(this) == -1)
				this.canChat = true
				
character
	var charName
	var team
	var stats = [5, 300, 24, 100, 0.12, 100, 0.12, 100, 0.12, 5, 10, 16, 8] // Attack, Attack Speed, Attack Range, Health, Health Regen, Energy, Energy Regen, Stamina, Stamina Regen, Move, Run Speed, KnockX, KnockY.
	var skills = []
	function canSelect(pClient)
		if(teamBalance)
			switch(team)
				case "hero": // Can only pick heroes or villains if they have the same, or less than, the number of heroes/villains.
					if(matchManager.getMatch(pClient.matchID).teams[0] <= matchManager.getMatch(pClient.matchID).teams[1])
						return true
					else
						return false
					break
				case "villain":
					if(matchManager.getMatch(pClient.matchID).teams[1] <= matchManager.getMatch(pClient.matchID).teams[0])
						return true
					else
						return false
					break
				case "akatsuki": // Can only pick Akatsuki if they have the same, or less than, the number of heroes and villains.
					if(matchManager.getMatch(pClient.matchID).teams[2] <= matchManager.getMatch(pClient.matchID).teams[0] && matchManager.getMatch(pClient.matchID).teams[2] <= matchManager.getMatch(pClient.matchID).teams[1])
						return true
					else
						return false
					break
				default:
					return true
		return true
	Naruto
		team = "hero"
		skills = ["Generic"]
		charName = "Naruto"
	Sasuke
		team = "villain"
		charName = "Sasuke"