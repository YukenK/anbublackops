Client
	mainOutput = 'Interface.chatBox'
	var chatBox
	var chatInput
	var isChatting
	var selectStats
	var currentScreen
	var canChat
	var keyBinds = {90: "JUMP", 88: "RUN", 39: "RIGHT", 37: "LEFT", 81: "SKILL1", 87: "SKILL2", 69: "SKILL3", 82: "SKILL4", 65: "SKILL5", 68: "SKILL6", 83: "SKILL7", 38: "UP", 40: "DOWN", 67: "ATTACK"}
	var isHeld = {}
	onNew()
		foreach(var M in matchManager.matches)
			foreach(var P in M.players)
				if(P.clientName = this.getAccountName())
					this.mob = P
	command enterKey()
		if(!this.isChatting && this.canChat)
			this.isChatting = true
			this.setFocus(this.chatInput)
			this.chatBox.chatFocus(this)
			this.chatInput.chatFocus()
	function showText(pText)
		this.outputText(pText)
	function setKey(k, type)
		this.keyBinds[k] = type
	function resetScreens()
		this.hideInterface("Interface")
		this.showInterface('backgroundInterface')
		this.showInterface('teamScreen')
		this.currentScreen = "teamScreen"
		foreach(var T in this.getInterfaceElements('heroScreen'))
			T.setTransition({'alpha': 0})
			T.mouseOpacity = 0
			if(T.parentType == "Interface/characterSelect")
				T.selectDisplay.setTransition({'alpha': 0})
		foreach(var T in this.getInterfaceElements('villainScreen'))
			T.setTransition({'alpha': 0})
			T.mouseOpacity = 0
			if(T.parentType == "Interface/characterSelect")
				T.selectDisplay.setTransition({'alpha': 0})
		foreach(var T in this.getInterfaceElements('akatsukiScreen'))
			T.setTransition({'alpha': 0})
			T.mouseOpacity = 0
			if(T.parentType == "Interface/characterSelect")
				T.selectDisplay.setTransition({'alpha': 0})
		spawn(100)
			this.showInterface('heroScreen')
			this.showInterface('villainScreen')
			this.showInterface('akatsukiScreen')
	onKeyDown(k)
		if(this.keyBinds[k] && !this.isHeld[this.keyBinds[k]])
			this.isHeld[this.keyBinds[k]] = true
			switch(this.keyBinds[k])
				case "JUMP":
					this.mob.jump()
					break
				case "RUN":
					this.mob.isRun = true
					break
				case "SKILL1":
					this.mob.hotKey(0)
					break
				case "SKILL2":
					this.mob.hotKey(1)
					break
				case "SKILL3":
					this.mob.hotKey(2)
					break
				case "SKILL4":
					this.mob.hotKey(3)
					break
				case "SKILL5":
					this.mob.hotKey(4)
					break
				case "SKILL6":
					this.mob.hotKey(5)
					break
				case "SKILL7":
					this.mob.hotKey(6)
					break
				case "UP":
					this.mob.wallRun(true)
					break
				case "DOWN":
					this.mob.wallRun(false)
					break
				case "ATTACK":
					this.mob.Attack()
					break
	onKeyUp(k)
		if(this.keyBinds[k] && this.isHeld[this.keyBinds[k]])
			this.isHeld[this.keyBinds[k]] = false
			if(this.keyBinds[k] == "RUN")
				this.mob.isRun = false