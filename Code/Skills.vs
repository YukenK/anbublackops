Mob
	var skills = []
	var passives = []
	var deathPassives = []
	var killPassives = []
	var meleePassives = [] // Upon hitting an enemy.
	var hitPassives = [] // Upon being hit.
	var cooldowns = []
	function hotKey(k)
		this.skills[k].Use(this, "SKILL" + (k + 1))
skill
	var cooldownTime
	var cooldownID
	var castTime // How long this binds you while casting.
	var castIcon
	var castIconState
	var castTurn = false
	var castDamage = false
	var skillTime // How long you are bound after casting.
	var skillIcon
	var skillIconState
	var skillTurn
	var skillDamage
	var multiCast // Related to skillTime, if this ability casts multiple times (IE. multiple fireballs so long as you are still bound via skillTime).
	var multiCastDelay
	
	
	
	var energyCost
	var staminaCost
	var chargeTime
	var skillThread
	var chargeMaxOnly = true // If this is false, a skill can be used if not fully charged, it will send the current charge percentage.
	var chargeCost // How much energy this costs per charge tick, which occur 20 times a second.
	var chargeStaminaCost
	var chargeBind = true // If charging a skill binds you in place.
	var chargeDamage = false
	var chargeTurn = false
	var chargeIcon
	var chargeIconState
	function canUse(user)
		if(user.dead || eventPlayers.indexOf(user) == -1 || user.binds.length)
			return false
		if(user.cooldowns[this.cooldownID] > Date.now())
			return false
		if(this.chargeCost)
			var totalCost = (this.chargeTime / 50 * this.chargeCost) + this.energyCost
			if(user.energy < totalCost)
				return false
		if(this.chargeStaminaCost)
			var totalCost = (this.chargeTime / 50 * this.chargeStaminaCost) + this.staminaCost
			if(user.stamina < totalCost)
				return false
		if(user.energy < this.energyCost)
			return false
		return true
	function Use(user, held)
		if(!this.canUse(user)) return
		if(this.chargeTime)
			var startTime = Date.now()
			var endTime = Date.now() + chargeTime
			var prematureEnd = false
			var bind
			if(this.chargeBind)
				bind = user.addBind(100, this.chargeTurn, this.chargeIcon, this.chargeDamage, this.chargeIconState)
			this.skillThread = thread(50)
				if(!prematureEnd && this.canUse(user) && user.client.isHeld[held])
					if(this.chargeBind && user.binds.indexOf(bind) != -1)
						bind.endTime = Date.now() + 100
					else
						user.removeBind(bind)
						prematureEnd = true
					if(this.chargeCost)
						user.energyChange(-this.chargeCost)
					if(this.chargeStaminaCost)
						user.staminaChange(-this.chargeStaminaCost)
					if(Date.now() >= endTime)
						var percentCharge = (endTime - startTime) + (Date.now() - startTime)
						if(this.chargeBind && user.binds.indexOf(bind) != -1)
							user.removeBind(bind)
						this.Used(user, percentCharge)
						Event.interruptThread(this.skillThread)
				else
					if(!this.chargeMaxOnly)
						var percentCharge = (endTime - startTime) + (Date.now() - startTime)
						if(this.chargeBind && user.binds.indexOf(bind) != -1)
							user.removeBind(bind)
						this.Used(user, percentCharge)
					Event.interruptThread(this.skillThread)
		else
			this.Used(user)
	function Used(user, charge)
		var endTime
		var bind
		var skillBind
		var castEnd = false
		var skillBound = false
		var nextMultiCast
		user.cooldowns[this.cooldownID] = Date.now() + this.cooldownTime
		if(!this.castTime && !this.skillTime)
			this.Cast(user, charge)
		else
			if(this.castTime)
				endTime = Date.now() + this.castTime
				bind = user.addBind(this.castTime + 100, this.castTurn, this.castIcon, this.castDamage, this.castIconState)
			this.skillThread = thread(50)
				if(user.getBind("stun"))
					if(bind)
						user.removeBind(bind)
					Event.interruptThread(this.skillThread)
				if(this.castTime && !castEnd)
					if(user.binds.indexOf(bind) == -1)
						Event.interruptThread(this.skillThread)
					if(Date.now() > endTime)
						castEnd = true
						this.Cast(user, charge)
						nextMultiCast = Date.now() + this.multiCastDelay
						user.removeBind(bind)
						if(!this.skillTime)
							Event.interruptThread(this.skillThread)
				if(this.skillTime)
					if(this.castTime && castEnd && !skillBound)
						skillBound = true
						bind = user.addBind(this.skillTime, this.skillTurn, this.skillIcon, this.skillDamage, this.skillIconState)
					else if(!this.castTime && !skillBound)
						skillBound = true
						bind = user.addBind(this.skillTime, this.skillTurn, this.skillIcon, this.skillDamage, this.skillIconState)
					if(skillBound && this.multiCast && Date.now() >= nextMultiCast && user.binds.indexOf(bind) != -1)
						this.Cast(user, charge)
						nextMultiCast = Date.now() + this.multiCastDelay
					else if(skillBound && user.binds.indexOf(bind) == -1)
						Event.interruptThread(this.skillThread)
	function Cast(user, charge)