event
	wallRun
		doesTick = 1
		eventID = "wallRun"
		tickTime = 50
		function Ticked()
			if(this.owners[0].canWallRun())
				this.owners[0].energyChange(-0.32)
			else
				this.owners[0].removeEvent(this)
Mob
	function canWallRun()
		if(!this.front(4, false, true).length)
			return false
		if(this.energy < 5)
			return false
		return this.canMove()
	function wallRun(dir)
		if(!this.getEvent("wallRun") && this.canWallRun())
			this.addEvent("event/wallRun", [this])
		if(this.canWallRun())
			if(dir && this.canWallRun()) // UP
				this.wallDir = "UP"
				this.wallVelocity.y = -this.runSpeed
			else
				this.wallDir = "DOWN"
				this.wallVelocity.y = this.runSpeed