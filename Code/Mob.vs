Mob
	moveSettings = { 'stepSize': 2, 'stepGlide': 0, 'stepSlide': false, 'allowFloat': false }
	var moveSpeed = 5
	var runSpeed = 10
	var jumpRate = -20
	var gravRate = 1 // The rate at which their velocity is changed.
	var grav = 5 // The constant rate applied to them.
	var velocity
	var wallVelocity // Used for jumping left/right from a wall.
	var forceVelocities = [] // For skills to add velocities to a player.
	var moveX = 0
	var isRun = false
	var isGrounded = false
	var selectedHero = false
	var charName
	var matchID
	var clientName
	var wallDir
	atlasName = 'devTest'
	iconName = "playerIcon"
	iconState = "standeast"
	density = 1
	transform = {'moveY': -64}
	dir = "east"
	onLogin()
		var loginThread = thread(50)
			if(this.client)
				this.clientName = this.client.getAccountName()
				this.client.chatBox = this.client.getInterfaceElement('Interface', 'chatBox')
				this.client.chatBox.chatUnfocus()
				this.client.chatInput = this.client.getInterfaceElement('Interface', 'chatInput')
				this.client.chatInput.chatUnfocus()
				this.staminaBar = this.client.getInterfaceElement("HUD", 'Stamina')
				this.energyBar = this.client.getInterfaceElement("HUD", 'Energy')
				this.healthBar = this.client.getInterfaceElement("HUD", "Health")
				if(!this.selectedHero)
					this.client.resetScreens()
				else
					this.client.matchID = this.matchID
					matchManager.getMatch
				Event.interruptThread(loginThread)
	onNew()
		this.velocity = new Object('vector', 0, 0)
		this.wallVelocity = new Object('vector', 0, 0)
	function acceleration()
		if(this.client)
			if(this.client.isHeld["LEFT"] && this.client.isHeld["RIGHT"])
				this.moveX = 0
			else if(this.client.isHeld["LEFT"])
				this.moveX = -1
			else if(this.client.isHeld["RIGHT"])
				this.moveX = 1
			else
				this.moveX = 0
			if(this.client.isHeld["UP"] && !this.client.isHeld["DOWN"])
				this.wallRun(true)
			else if(this.client.isHeld["DOWN"] && !this.client.isHeld["UP"])
				this.wallRun(false)
			else
				this.wallVelocity.y = 0
		if(this.canTurn())
			if(this.moveX == 1)
				this.setDir("east")
			else if(this.moveX == -1)
				this.setDir("west")
		if(this.canRun() && this.isRun)
			this.velocity.x = this.runSpeed * this.moveX
		else
			this.velocity.x = this.moveSpeed * this.moveX
	function gravity()
		if(this.velocity.y > 0)
			this.velocity.y = clamp(this.velocity.y - this.gravRate, 0, 100)
		else if(this.velocity.y < 0)
			this.velocity.y = clamp(this.velocity.y + this.gravRate, -100, 0)
		if(this.wallVelocity.x > 0)
			this.wallVelocity.x = clamp(this.wallVelocity.x - this.gravRate, 0, 100)
		else if(this.wallVelocity < 0)
			this.wallVelocity.x = clamp(this.wallVelocity.x + this.gravRate, -100, 0)
		if(!this.getEvent("wallRun"))
			this.stepPos(0, this.grav)
		var i = 0
		foreach(var T in Map.getTilesByRange(this, 0, 8))
			if(T.density && T.yPos > this.yPos)
				this.isGrounded = true
				i++
			else if(T.density && T.yPos < this.yPos)
				this.velocity.y = 0
		if(!i)
			this.isGrounded = false
	function move()
		var vec = new Object('vector', 0, 0)
		if(this.canMove())
			vec.x += this.velocity.x
			vec.y += this.velocity.y
			if(this.moveX != 0 && this.isRun && this.canRun())
				this.staminaChange(-0.05)
		if(this.canForceMove())
			foreach(var V in this.forceVelocities)
				vec.x += V.x
				vec.y += V.y
		if(this.getEvent("wallRun"))
			vec.y += this.wallVelocity.y
			vec.x += this.wallVelocity.x
		this.stepPos(vec.x, 0)
		this.stepPos(0, vec.y)
	function jump()
		if(this.canJump())
			this.staminaChange(-10)
			this.velocity.y = this.jumpRate
			if(this.getEvent("wallRun"))
				if(this.dir == "east")
					this.wallVelocity.x = this.jumpRate
					this.dir = "west"
				else
					this.wallVelocity.x = -this.jumpRate
					this.dir = "east"
	function canMove()
		if(this.binds.length)
			return false
		return !this.dead
	function canForceMove() // If forced movement will apply.
		return true
	function canRun()
		if(this.stamina < 5)
			return false
		return this.canMove()
	function canJump()
		if(this.stamina < 10 || !this.isGrounded && !this.getEvent("wallRun"))
			return false
		return this.canMove()
	function canTurn()
		if(this.binds.length)
			foreach(var B in this.binds)
				if(!B.canTurn)
					return false
		return true
	function iconStates()
		if(this.dead)
			this.iconState = "dead"
		else if(this.getBind("stun"))
			this.iconState = "stun" + this.dir
		else if(this.getEvent("wallRun"))
			this.iconState = "wallrun" + this.wallDir
		else if(!this.isGrounded)
			this.iconState = "jump" + this.dir
		else if(this.moveX && this.isRun && this.canRun())
			this.iconState = "run" + this.dir
		else if(this.moveX && this.canMove())
			this.iconState = "move" + this.dir
		else
			this.iconState = "stand" + this.dir
		foreach(var B in this.binds)
			if(B.iconState)
				this.iconState = B.iconState
			