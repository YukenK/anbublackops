var eventPlayers = []
function eventLoop()
	var eventLoop = thread(20) // 50 ticks a second.
		foreach(var M in eventPlayers)
			foreach(var E in M.events)
				if(E.doesTick && E.canTick() && Date.now() >= E.nextTick)
					E.Tick()
			M.eventLoop()
			M.moveLoop()
			M.iconStates()
Mob
	var events = []
	function addEvent(type, _owners, _doesTick, _endTick, _endTime = -1, _ownerRemove, _tickTime, _other, _id) // Other is an array that some events might use onNew.
		var E = new Diob(type, _owners, _doesTick, _endTick, _endTime, _ownerRemove, _tickTime, _other, _id)
		this.events.push(E)
		foreach(var M in _owners)
			if(M != this)
				M.events.push(E)
	function getEvent(_id)
		foreach(var E in this.events)
			if(E.eventID == _id)
				return E
		return false
	function removeEvent(event)
		this.events.splice(event, 1)
		if(event.ownerRemove)
			foreach(var P in event.owners)
				if(P != this)
					P.events.splice(event, 1)
	function eventCheck()
		foreach(var E in this.events)
			if(E.endTime != -1 && Date.now() >= this.endTime)
				this.removeEvent(E)
	function eventLoop()
		this.eventCheck()
		this.bindCheck()
		this.buffCheck()
		this.Regeneration()
	function moveLoop()
		this.gravity()
		this.acceleration()
		this.move()
event
	var doesTick
	var nextTick = 0
	var ticks // How many times this has ticked.
	var endTick // How many times this should tick, if it does tick.
	var endTime
	var tickTime
	var owners = []
	var ownerRemove // Remove this if one of the original owners no longer has it in their events.
	var eventID
	onNew(_owners, _doesTick, _endTick, _endTime = -1, _ownerRemove, _tickTime, _other, _id)
		if(_doesTick)
			this.doesTick = _doesTick
		if(_endTick)
			this.endTick = _endTick
		if(_endtime)
			this.endTime = _endTime
		if(_id)
			this.eventID = _id
		if(_other)
			this.setOther(_other)
		this.owners = _owners
		this.ownerRemove = _ownerRemove
	function setOther(_other)
	function canTick()
		var endEvent
		if(this.ownerRemove)
			foreach(var M in this.owners)
				if(M.events.indexOf(this) == -1)
					endEvent++
		if(endEvent)
			foreach(var M in this.owners)
				if(M.events.indexOf(this) != -1)
					M.events.splice(this, 1)
			return false
		return true
	function Tick()
		this.ticks++
		if(this.ticks > this.endTick)
			foreach(var M in this.owners)
				M.removeEvent(this)
		else if(Date.now() >= this.nextTick)
			this.nextTick = this.tickTime + Date.now()
			this.Ticked()
	function Ticked()
	function endTick()
		foreach(var M in this.owners)
			if(M.events.indexOf(this) != -1)
				M.events.splice(this, 1)