Diob
	function front(i, _wallCheck, _wallsOnly)
		retVal = []
		if(!_wallsOnly)
			foreach(var M in Map.getRange(this, i, 0))
				if(this.dir == "east" && M.xPos > this.xPos)
					retVal.push(M)
				else if(this.dir == "west" && M.xPos < this.xPos)
					retVal.push(M)
		if(_wallCheck || _wallsOnly)
			var walls = []
			foreach(var W in Map.getTilesByRange(this, i, 0)) // Push all tiles that are above 5 density.
				if(W.density > 5)
					if(this.dir == "east" && W.xPos > this.xPos)
						walls.push(W)
					else if(this.dir == "west" && W.xPos < this.xPos)
						walls.push(W)
			// Check if a wall comes before a diob in ret. If it does, remove that.
			if(!_wallsOnly)
				foreach(var M in retVal)
					foreach(var W in walls)
						if(this.dir == "east" && M.xPos > this.xPos && W.xPos < M.xPos) // If you're facing east & they're east of you, but a wall comes before them. 
							retVal.splice(M, 1)
						else if(this.dir == "west" && M.xPos < this.xPos && W.xPos > M.xPos)
							retVal.splice(M, 1)
			else
				retVal = walls