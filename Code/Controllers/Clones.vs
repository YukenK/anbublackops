cloneController // A controller for clones.
cloneAI // The actual clones.
	var behaviorType = "Aggressive" // Aggressive (follows the nearest enemies), Defensive (follows its creator, attacking enemies near them).
	var openNodes = []
	var closedNodes = []
	var target
	var selectedNode
	function walkTo()
	function getNodes()
		retVal = []
		foreach(var T in Map.getTilesByRange(this, Map.getDist(this, this.target))) // Grab all tiles from this to this.target.
			if(!T.density) // If not dense, it is walkable.
				retVal.push(T)
	function getPath() // Get the shortest path
		this.openNodes = this.getNodes()
		foreach(var T in this.openNodes)
			