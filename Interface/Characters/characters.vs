over
	mouseOpacity = 0
	atlasName = "Avatars"
	iconName = "defaultAvatar"
	alpha = 0
select
	mouseOpacity = 0
	atlasName = "Avatars"
	iconName = "selectedHero"
	alpha = 0
Client
	var currentScreen
	function switchScreen(currentScreen, newScreen)
		if(currentScreen)
			foreach(var T in this.getInterfaceElements(currentScreen))
				T.mouseOpacity = 0
				T.setTransition({'alpha': 0}, 8, 20)
				if(T.parentType == 'Interface/characterSelect')
					T.unshowCharacter(this)
		spawn(20)
			if(newScreen)
				foreach(var T in this.getInterfaceElements(newScreen))
					T.mouseOpacity = 1
					T.setTransition({'alpha': 1}, 8, 20)
					if(T.parentType == 'Interface/characterSelect')
						T.showCharacter(this)
					this.currentScreen = newScreen
Interface
	characterBackground
		atlasName = 'Avatars'
		iconName = "Select"
	characterInterface
		atlasName = "Avatars"
		width = 420
		height = 240
		var screen

		onMouseClick(pClient)
			pClient.switchScreen(pClient.currentScreen, this.screen)
		akatsukiSelect
			screen = "akatsukiScreen"
			iconName = "akatsukiSelect"
		heroSelect
			screen = "heroScreen"
			iconName = "heroSelect"
		villainSelect
			screen = "villainScreen"
			iconName = "villainSelect"
		backButton
			width = 96
			height = 32
			iconName = "backButton"
			mainMenu
				screen = "teamScreen"
	characterSelect
		atlasName = 'Avatars'
		iconName = "defaultAvatar"
		var overDisplay
		var selectDisplay
		var charName
		function showCharacter(pClient)
			if(matchManager.getMatch(pClient.matchID).selectedHeroes.indexOf(this.iconName) == -1)
				this.selectDisplay.setTransition({'alpha': 0})
			else
				this.selectDisplay.setTransition({'alpha': 0.6}, 8, 20)
		function unshowCharacter(pClient)
			if(matchManager.getMatch(pClient.matchID).selectedHeroes.indexOf(this.iconName) != -1)
				this.selectDisplay.setTransition({'alpha': 0}, 8, 20)
			
		onNew()
			this.overDisplay = new Diob('over')
			this.selectDisplay = new Diob('select')
			this.addOverlay(this.overDisplay)
			this.addOverlay(this.selectDisplay)
		onMouseClick(pClient)
			pClient.selectHero(this.iconName)
		onMouseEnter(pClient)
			if(matchManager.getMatch(pClient.matchID).selectedHeroes.indexOf(this.iconName) == -1)
				this.overDisplay.setTransition({'alpha': 0.25}, 8, 20)
		onMouseExit(pClient)
			if(matchManager.getMatch(pClient.matchID).selectedHeroes.indexOf(this.iconName) == -1)
				this.overDisplay.setTransition({'alpha': 0}, 8, 20)
		Naruto
			iconName = "Naruto"
		Sasuke
			iconName = "Sasuke"