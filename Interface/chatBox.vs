Interface
	chat
		chatBox
			width = 300
			height = 200
			atlasName = 'interfaceAtlas'
			iconName = "chatBox"
			textStyle = {'fontSize':15,'fontFamily':'Arial','padding':4,'fill':'#FFFFFF'}
			interfaceType = "WebBox"
			mouseOpacity = 0
		chatInput
			width = 300
			height = 30
			atlasName = 'interfaceAtlas'
			iconName = "chatInput"
			textStyle = {'fontSize':15,'fontFamily':'Arial','padding':4,'fill':'#FFFFFF'}
			interfaceType = "CommandInput"
			mouseOpacity = 0
			onExecute(pClient)
				var T = this.getText()
				pClient.isChatting = false
				pClient.chatBox.chatUnfocus()
				pClient.setFocus()
				pClient.chatInput.chatUnfocus()
				if(T)
					foreach(var P in matchManager.getMatch(pClient.matchID).players)
						if(P.client)
							P.client.showText(pClient.mob.charName + ' (' + pClient.name +')' + ' says: ' + T)
		function chatFocus(pClient)
			if(pClient)
				pClient.isHeld = {}
			this.setTransition({'alpha': 1}, 8, 20)
		function chatUnfocus()
			this.setTransition({'alpha': 0.5}, 8, 20)